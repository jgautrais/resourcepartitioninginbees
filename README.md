# ResourcePartitioningInBees

Source code for the companion paper:
A simple model for the emergence of resource partitioning between traplining bees, 2020
Pasquaretta et al 2020
CRCA, Univ. Toulouse, France